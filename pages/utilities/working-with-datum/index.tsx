import {
    Box, Heading
  } from '@chakra-ui/react'
  import type { NextPage } from "next";
import DatumHashStudyComponent from '../../../components/datumStudy/DatumHashStudyComponent';

  const WorkingWithDatumPage: NextPage = () => {


    return (
      <Box>
        <Heading>
            Working with Datum
        </Heading>
        <DatumHashStudyComponent />
      </Box>


    )
  }

  export default WorkingWithDatumPage
