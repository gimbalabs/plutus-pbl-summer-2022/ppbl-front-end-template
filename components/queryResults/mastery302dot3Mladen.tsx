import { useQuery, gql } from "@apollo/client";

import {
    Flex, Center, Heading, Text, Box, Link
} from "@chakra-ui/react";

// ------------------------------------------------------------------------
// Module 302, Mastery Assignment #3
//
// STEP 1: Replace this GraphQL query with a new one that you write.
//
// Need some ideas? We will brainstorm at Live Coding.
// ------------------------------------------------------------------------
const QUERY = gql`
query {
    transactions
    (where :
        {outputs :
          {address :
            {_eq : "addr_test1qqr585tvlc7ylnqvz8pyqwauzrdu0mxag3m7q56grgmgu7sxu2hyfhlkwuxupa9d5085eunq2qywy7hvmvej456flknswgndm3"}}
          _and :
          { metadata :
            {key :
              {_eq : "721"}}}}){
      metadata{
        value
      }
      inputs {address}
    }
  }
`;

export default function Mastery302dot3Template() {

    const queryAddress = "addr_test1qqr585tvlc7ylnqvz8pyqwauzrdu0mxag3m7q56grgmgu7sxu2hyfhlkwuxupa9d5085eunq2qywy7hvmvej456flknswgndm3"

    // EXAMPLE WITH VARIABLE
    const { data, loading, error } = useQuery(QUERY, {
        variables: {
            address: queryAddress
        }
    });

    // EXAMPLE WITHOUT VARIABLE
    // const { data, loading, error } = useQuery(QUERY);

    if (loading) {
        return (
            <Heading size="lg">Loading data...</Heading>
        );
    };

    if (error) {
        console.error(error);
        return (
            <Heading size="lg">Error loading data...</Heading>
        );
    };

    // ------------------------------------------------------------------------
    // Module 302, Mastery Assignment #3
    //
    // STEP 2: Style your query results here.
    //
    // This template is designed be a simple example - add as much custom
    // styling as you want!
    // ------------------------------------------------------------------------

    return (
        <Box p="3" bg="orange.100" border='1px' borderRadius='lg'>
            <Heading py='2' size='md'>Address that sent Tx to faucet with metadata</Heading>
            <Text p='1' fontWeight='bold'>Tx that was sent to {queryAddress}</Text>
            <Text p="1" fontWeight='bold'>Sending address {data.transactions[0].inputs[0].address}</Text>
            {/* <Text p='1'>It was Tx: {data.transactions[0].hash}</Text>
            <Text p='1'>Date: {data.transactions[0].includedAt}</Text>
            <Text p='1'>Number Inputs: {data.transactions[0].inputs.length} -- Number Outputs: {data.transactions[0].outputs.length}</Text> */}
        </Box>
    )
}
