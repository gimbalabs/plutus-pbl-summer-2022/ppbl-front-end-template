// Imports:
// You may not use all of these, and you may need to add a few!
import {createRef, useState } from "react";
import {Box, Heading, Text, Button, Link} from "@chakra-ui/react"
import { Asset, Transaction, UTxO } from "@meshsdk/core";
import { useAddress, useWallet } from "@meshsdk/react";

export default function TransactionSergi10() {
    // These will come in handy:
    const { wallet } = useWallet();
    const address = useAddress();
    const [loading, setLoading] = useState(false);
    const [balance, setBalace] = useState<null | Asset[]>(null);
    const [walletUtxos, setUtxoList] = useState<null | UTxO[]>(null);
    const [collateralTX, setCurrentCollateral] = useState<null | UTxO[]>(null);

    async function getWalletUtxos() {
        if (wallet && address) {
            setLoading(true)
            const _utxos = await wallet.getUtxos();
            setUtxoList(_utxos)
            setLoading(false)
        }
    };

    async function getWalletData() {
        if (wallet && address) {
            setLoading(true)
            const _balance = await wallet.getBalance();
            setBalace(_balance)
            // console.log("getWalletData _balance" , _balance)
            setLoading(false)
        }
    };

    async function checkWalletCollateral() {
        if (wallet && address) {
            setLoading(true)
            const _collateral = await wallet.getCollateral();
            setCurrentCollateral(_collateral)
            setLoading(false)
        }
    };

    async function createCollateralTx() {
        if (wallet && address) {
            setLoading(true)
            // Create draftTX
            const tx = new Transaction({ initiator: wallet }).sendLovelace(address, '5000000')
            const unsignedTx = await tx.build()
            // Sign draftTX
            const signedTx = await wallet.signTx(unsignedTx)
            // Send signedTX
            const txHash = await wallet.submitTx(signedTx)
            setLoading(false)
            alert(txHash) 
        }
    };

    return (
        <Box p='5' bg='orange.100' border='1px' borderRadius='xl' fontSize='lg'>
                <Heading size='xl'>
                    Create collateral Tx
                </Heading>

                <Text py='3'>
                Tx Sergi10
                </Text>
                <Text py='3'>
                Check if you have a collateral transaction:
                </Text>
              
                <Button id="check_Collateral" onClick={checkWalletCollateral} colorScheme='cyan' my='3'>Check Collateral</Button>
                {(collateralTX) ? <div>
                        <p>Your collateral TX is: </p> <p>{collateralTX[0].input.txHash}</p> 
                        <Text pt='3' display={'inline'}>
                        View transaction on <Link href={`https://preprod.cardanoscan.io/transaction/${collateralTX[0].input.txHash}`}>https://preprod.cardanoscan.io/transaction/{collateralTX[0].input.txHash}/</Link>
                        </Text>
                    </div>: ""
                }

                <Text>
                Remember, if your transaction fails, you may lose the entire amount deposited as collateral.
                For this reason we will create a transaction with 5 ₳.
                </Text>
                <Button id="create_Collateral" onClick={createCollateralTx} colorScheme='orange' my='3'>Create Collateral Tx</Button>

                <Text>
                Show all transaction in your connected wallet:
                </Text>

                <Button id="btn2" onClick={getWalletUtxos} colorScheme='green' my='3'>Show transactions</Button>
                {walletUtxos ? ( walletUtxos.map((tx: UTxO) =>(
                    <ul>
                    <li>{tx.input.txHash}<b> # {tx.input.outputIndex}</b></li>
                    </ul>
                    )) ) : ""}
                
                <Button id="btn2" onClick={getWalletData} colorScheme='green' my='3'>Show wallet balance</Button>
                {balance ? ( balance.map((bl: Asset) =>(
                    <ul>
                    <li key={bl.unit}>{bl.quantity} - {bl.unit}</li>
                    </ul>
                    )) ) : ""}
        </Box>
    );
}